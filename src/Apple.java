package snake;

import javax.swing.*;
import java.awt.*;

public class Apple {

    public Position position;
    public Image appleImage;

    public Apple() {
        this.position = new Position();
        ImageIcon iia = new ImageIcon("src/resources/apple.png");
        appleImage = iia.getImage();
    }
}
