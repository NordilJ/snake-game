package snake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

public class Board extends JPanel implements ActionListener {

    public static final int BOARD_WIDTH = 405;
    public static final int BOARD_HEIGHT = 405;
    public static final int COMPONENT_SIZE = 15;
    public static final int IN_GAME_SCORE = 10;

    public Snake snake;
    public Apple apple;

    private boolean inGame = true;

    private int score = 0;

    private Timer timer;
    public int ADDITIONAL_DELAY = 110;


    public Board() {
        this.init();
    }

    private void init() {
        this.addKeyListener(new TAdapter());
        this.setBackground(Color.YELLOW);
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(BOARD_WIDTH, BOARD_HEIGHT));

        this.snake = new Snake();
        this.apple = new Apple();
        this.initGame();
    }

    private void initGame() {
        snake.headPosition.x = ((BOARD_WIDTH / COMPONENT_SIZE) / 2) * COMPONENT_SIZE;
        snake.headPosition.y = ((BOARD_HEIGHT / COMPONENT_SIZE) / 2) * COMPONENT_SIZE;

        for (int i = 1, x = COMPONENT_SIZE; i <= 3; i++, x += COMPONENT_SIZE) {
            Position position = new Position();
            position.x = snake.headPosition.x + x;
            position.y = snake.headPosition.y;
            snake.bodyPositions.add(position);
        }

        Position applePosition = generateAppleNewPosition();
        apple.position = applePosition;

        timer = new Timer(ADDITIONAL_DELAY, this);
        timer.start();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        this.move();
        this.checkApple();
        this.checkCollision();
        this.doDrawing(g);
    }

    private void checkCollision() {
        if (snake.headPosition.x < 0 || snake.headPosition.x > BOARD_WIDTH || snake.headPosition.y < 0 || snake.headPosition.y >BOARD_HEIGHT) {
            this.inGame = false;
        }

        if (snake.bodyPositions.stream().anyMatch(body -> body.x == snake.headPosition.x && body.y == snake.headPosition.y)) {
            this.inGame = false;
        }
    }

    private void checkApple() {
        if (snake.headPosition.x == apple.position.x && snake.headPosition.y == apple.position.y) {
            Position position = generateAppleNewPosition();
            apple.position = position;
            score += IN_GAME_SCORE;
            if (this.score % (IN_GAME_SCORE * 10) == 0){
                ADDITIONAL_DELAY -= 20;
                timer.stop();
                timer = new Timer(ADDITIONAL_DELAY, this);
                timer.start();
            }
        }
    }

    private void move() {
        Position tail = new Position();
        tail.x = snake.headPosition.x;
        tail.y = snake.headPosition.y;

        switch (snake.direction) {
            case UP:
                snake.headPosition.y -= COMPONENT_SIZE;
                break;
            case DOWN:
                snake.headPosition.y += COMPONENT_SIZE;
                break;
            case LEFT:
                snake.headPosition.x -= COMPONENT_SIZE;
                break;
            case RIGHT:
                snake.headPosition.x += COMPONENT_SIZE;
                break;
        }

        snake.direction = snake.newDirection;

        if (snake.headPosition.x != apple.position.x || snake.headPosition.y != apple.position.y) {
            snake.bodyPositions.pollLast();
        }

        snake.bodyPositions.add(0, tail);
    }

    private Position generateAppleNewPosition() {
        Random rng = new Random();
        boolean positionIsOk = false;
        Position position = new Position();
        while (!positionIsOk){
            position.x = rng.nextInt(BOARD_WIDTH / COMPONENT_SIZE) * COMPONENT_SIZE;
            position.y = rng.nextInt(BOARD_HEIGHT / COMPONENT_SIZE) * COMPONENT_SIZE;
            if (snake.headPosition.x == position.x && snake.headPosition.y == position.y) {
                continue;
            }

            if (snake.bodyPositions.stream().anyMatch(body -> body.x == position.x && body.y == position.y)){
                continue;
            }

            positionIsOk = true;
        }

        return position;
    }

    private void doDrawing(Graphics g) {
        if (this.inGame){
            g.drawImage(apple.appleImage, apple.position.x, apple.position.y, this);
            g.drawImage(snake.headImage, snake.headPosition.x, snake.headPosition.y, this);
            for (int i = 0; i < snake.bodyPositions.size(); i++) {
                g.drawImage(snake.bodyImage, snake.bodyPositions.get(i).x, snake.bodyPositions.get(i).y, this);
            }

            Toolkit.getDefaultToolkit().sync();
        } else {
            timer.stop();
            this.gameOver(g);
        }
    }

    private void gameOver(Graphics g) {
        String msg = "Game Over";
        Font small = new Font("Helvetica", Font.BOLD, 24);

        g.setColor(Color.BLACK);
        g.setFont(small);
        g.drawString(msg, BOARD_WIDTH  / 2, BOARD_HEIGHT / 2);

        String scoreMsg = "Score: " + this.score;
        g.drawString(scoreMsg, BOARD_WIDTH  / 2, (BOARD_HEIGHT / 2) + 20);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.repaint();
    }

    private class TAdapter extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT && snake.direction != Direction.RIGHT) {
                snake.newDirection = Direction.LEFT;
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT && snake.direction != Direction.LEFT) {
                snake.newDirection = Direction.RIGHT;
            } else if (e.getKeyCode() == KeyEvent.VK_DOWN && snake.direction != Direction.UP) {
                snake.newDirection = Direction.DOWN;
            }else if (e.getKeyCode() == KeyEvent.VK_UP && snake.direction != Direction.DOWN) {
                snake.newDirection = Direction.UP;
            }
        }
    }
}
