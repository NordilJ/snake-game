package snake;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

public class Snake {

    public Position headPosition;
    public LinkedList<Position> bodyPositions;
    public Direction direction;
    public Direction newDirection;
    public Image headImage;
    public Image bodyImage;

    public Snake() {
        this.headPosition = new Position();
        this.bodyPositions = new LinkedList<>();
        this.direction = Direction.LEFT;
        this.newDirection = Direction.LEFT;
        ImageIcon iih = new ImageIcon("src/resources/head.png");
        headImage = iih.getImage();
        ImageIcon iib = new ImageIcon("src/resources/body.png");
        bodyImage = iib.getImage();
    }


}
